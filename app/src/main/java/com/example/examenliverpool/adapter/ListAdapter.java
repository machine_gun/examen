package com.example.examenliverpool.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.examenliverpool.R;
import com.example.examenliverpool.dto.Records;
import com.example.examenliverpool.utils.Interfaces;
import com.example.examenliverpool.utils.Tools;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

    private Context context;
    private Interfaces.OnResponse<Object> onResponse;
    private int request;
    private List<Records> list;
    private DisplayImageOptions options;

    public ListAdapter(Context context, Interfaces.OnResponse<Object> onResponse, int request, List<Records> list) {
        this.context = context;
        this.onResponse = onResponse;
        this.request = request;
        this.list = list;
        options = Tools.getImageLoaderOptions(20, android.R.drawable.stat_sys_download);
    }



    public void addList(List<Records> list) {
        int size = this.list.size();
        this.list.addAll(list);
        notifyItemRangeChanged(size,list.size());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.row, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Records item = list.get(position);

        holder.tvTitle.setText(item.getProductDisplayName());
        holder.tvPrice.setText(String.format("$ %s", item.getListPrice()));
        holder.tvLocation.setText("");
        ImageLoader.getInstance().displayImage(item.getSmImage(), holder.thumbnail, options);
        holder.llRow.setTag(item);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvTitle;
        private TextView tvPrice;
        private TextView tvLocation;
        private ImageView thumbnail;
        private LinearLayout llRow;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvLocation = itemView.findViewById(R.id.tvLocation);
            thumbnail = itemView.findViewById(R.id.thumbnail);
            llRow = itemView.findViewById(R.id.llRow);
            llRow.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onResponse.onResponse(request, v.getTag());
        }
    }
}
