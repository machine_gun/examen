package com.example.examenliverpool;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.examenliverpool.adapter.ListAdapter;
import com.example.examenliverpool.db.RecordSQLiteOpenHelper;
import com.example.examenliverpool.dto.Records;
import com.example.examenliverpool.dto.ResponseDTO;
import com.example.examenliverpool.utils.CustomDialog;
import com.example.examenliverpool.utils.CustomListView;
import com.example.examenliverpool.utils.Interfaces;
import com.example.examenliverpool.ws.WebService;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

import java.util.List;

public class MainActivity extends AppCompatActivity implements Interfaces.OnResponse<Object> {

    private static final int REQUEST_CODE = 56;
    private static final int REQUEST_CODE_2 = 57;
    private static final int ITEM_CLICK = 58;

    private static int PAGE = 1;
    private static String busqueda = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);


        if (!ImageLoader.getInstance().isInited()) {
            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                    .threadPoolSize(3)
                    .threadPriority(Thread.NORM_PRIORITY - 2)
                    .tasksProcessingOrder(QueueProcessingType.FIFO)
                    .memoryCache(new WeakMemoryCache())
                    .memoryCacheSize(2 * 1024 * 1024)
                    .memoryCacheSizePercentage(13)
                    .diskCacheSize(50 * 1024 * 1024)
                    .diskCacheFileCount(100)
                    .diskCacheFileNameGenerator(new HashCodeFileNameGenerator())
                    .imageDownloader(new BaseImageDownloader(this))
                    .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
//                .writeDebugLogs()
                    .build();
            ImageLoader.getInstance().init(config);
        }

        initViews();


    }

    private void initViews() {


        final EditText et_search = findViewById(R.id.et_search);
        final TextView tv_tip = findViewById(R.id.tv_tip);

        Drawable drawable = getResources().getDrawable(R.drawable.search);
        drawable.setBounds(0, 0, 60, 60);
        et_search.setCompoundDrawables(drawable, null, null, null);


        et_search.setOnKeyListener(new View.OnKeyListener() {

            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN) {// 修改回车键功能

                    ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
                            getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                    String text = et_search.getText().toString().trim();
                    boolean hasData = hasData(text);
                    if (!hasData) {
                        insertData(text);
                        queryData("");
                    }

                    busqueda = text;
                    goToWS();

                }
                return false;
            }
        });

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().trim().length() == 0) {
                    tv_tip.setText("Buscar");
                } else {
                    tv_tip.setText("Resultados de la búsqueda");
                }
                ScrollView scrollView = findViewById(R.id.scrollView);
                scrollView.setVisibility(View.VISIBLE);
                String tempName = et_search.getText().toString();

                queryData(tempName);

            }
        });
        CustomListView listView = findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = view.findViewById(android.R.id.text1);
                String name = textView.getText().toString();
                et_search.setText("");
                et_search.append(name);
                busqueda = name;
                goToWS();

            }
        });
        tv_tip.setText("Historial");
        queryData("");

        RecyclerView recyclerView = findViewById(R.id.rv);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                    loadNextBlock();

                }
            }
        });


    }

    private void insertData(String tempName) {
        RecordSQLiteOpenHelper helper = new RecordSQLiteOpenHelper(this);
        SQLiteDatabase db = helper.getWritableDatabase();
        String query = "select count(*) from records where name = ' " + tempName + " '";
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            int count = c.getInt(0);
            if (count != 0) {
                return;
            }
        }


        query = " insert into records(name) values(' " + tempName + " ') ";
        db.execSQL(query);
        db.close();
    }

    private void queryData(String tempName) {
        RecordSQLiteOpenHelper helper = new RecordSQLiteOpenHelper(this);
        Cursor cursor = helper.getReadableDatabase().rawQuery(
                "select id as _id,name from records where name like '%" + tempName + "%' order by id desc ", null);

        BaseAdapter adapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1, cursor, new String[]{"name"},
                new int[]{android.R.id.text1}, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        CustomListView listView = findViewById(R.id.listView);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private boolean hasData(String tempName) {
        RecordSQLiteOpenHelper helper = new RecordSQLiteOpenHelper(this);
        Cursor cursor = helper.getReadableDatabase().rawQuery(
                "select id as _id,name from records where name =?", new String[]{tempName});

        return cursor.moveToNext();
    }

    private void goToWS() {
        ScrollView scrollView = findViewById(R.id.scrollView);
        scrollView.setVisibility(View.GONE);
        WebService.getSearch(this, REQUEST_CODE, busqueda, 1, "10");

    }

    private void loadNextBlock() {
        Toast.makeText(this,"Cargando más items",Toast.LENGTH_SHORT).show();
        WebService.getSearch(this, REQUEST_CODE_2, busqueda, ++PAGE, "10");
    }

    private void apendData(ResponseDTO data){
        RecyclerView recyclerView = findViewById(R.id.rv);
        ListAdapter adapter = (ListAdapter) recyclerView.getAdapter();


        adapter.addList(data.getPlpResults().getRecords());
    }

    private void loadData(ResponseDTO data) {
        RecyclerView recyclerView = findViewById(R.id.rv);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        ListAdapter adapter = new ListAdapter(this, this, ITEM_CLICK, data.getPlpResults().getRecords());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onResponse(int handlerCode, Object o) {


        switch (handlerCode) {
            case REQUEST_CODE:
                if (o == null) {
                    CustomDialog.show(this, null, 0, "Error al intentar conectarse al servidor, Por favor intente de nuevo más tarde");
                    return;
                }
                if (o instanceof String) {
                    CustomDialog.show(this, null, 0, (String) o);

                    return;
                }

                ResponseDTO data = (ResponseDTO) o;
                loadData(data);
                break;
            case ITEM_CLICK:

                Records item = (Records) o;

                Toast.makeText(this, item.getProductDisplayName(), Toast.LENGTH_SHORT).show();
                break;
            case REQUEST_CODE_2:

                if (o == null) {
                    return;
                }
                if (o instanceof String) {
                    return;
                }

                apendData((ResponseDTO) o);
                break;
        }


    }
}
