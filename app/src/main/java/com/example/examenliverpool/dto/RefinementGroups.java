package com.example.examenliverpool.dto;

import java.util.List;

public class RefinementGroups {
    private String name;

    private List<Refinement> refinement;

    private String dimensionName;

    private String multiSelect;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Refinement> getRefinement() {
        return refinement;
    }

    public void setRefinement(List<Refinement> refinement) {
        this.refinement = refinement;
    }

    public String getDimensionName() {
        return dimensionName;
    }

    public void setDimensionName(String dimensionName) {
        this.dimensionName = dimensionName;
    }

    public String getMultiSelect() {
        return multiSelect;
    }

    public void setMultiSelect(String multiSelect) {
        this.multiSelect = multiSelect;
    }
}
