package com.example.examenliverpool.dto;

public class ResponseDTO {
    private String pageType;

    private PlpResults plpResults;

    private Status status;

    public String getPageType ()
    {
        return pageType;
    }

    public void setPageType (String pageType)
    {
        this.pageType = pageType;
    }

    public PlpResults getPlpResults ()
    {
        return plpResults;
    }

    public void setPlpResults (PlpResults plpResults)
    {
        this.plpResults = plpResults;
    }

    public Status getStatus ()
    {
        return status;
    }

    public void setStatus (Status status)
    {
        this.status = status;
    }


}
