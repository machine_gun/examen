package com.example.examenliverpool.dto;

public class Refinement {

    private String refinementId;

    private String count;

    private String label;

    private String selected;

    public String getRefinementId ()
    {
        return refinementId;
    }

    public void setRefinementId (String refinementId)
    {
        this.refinementId = refinementId;
    }

    public String getCount ()
    {
        return count;
    }

    public void setCount (String count)
    {
        this.count = count;
    }

    public String getLabel ()
    {
        return label;
    }

    public void setLabel (String label)
    {
        this.label = label;
    }

    public String getSelected ()
    {
        return selected;
    }

    public void setSelected (String selected)
    {
        this.selected = selected;
    }
}
