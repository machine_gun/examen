package com.example.examenliverpool.dto;

import java.util.List;

public class PlpResults {
    private List<RefinementGroups> refinementGroups;

    private Navigation navigation;

    private List<SortOptions> sortOptions;

    private List<Records> records;

    private PlpState plpState;

    private String label;

    public List<RefinementGroups> getRefinementGroups() {
        return refinementGroups;
    }

    public void setRefinementGroups(List<RefinementGroups> refinementGroups) {
        this.refinementGroups = refinementGroups;
    }

    public Navigation getNavigation() {
        return navigation;
    }

    public void setNavigation(Navigation navigation) {
        this.navigation = navigation;
    }

    public List<SortOptions> getSortOptions() {
        return sortOptions;
    }

    public void setSortOptions(List<SortOptions> sortOptions) {
        this.sortOptions = sortOptions;
    }

    public List<Records> getRecords() {
        return records;
    }

    public void setRecords(List<Records> records) {
        this.records = records;
    }

    public PlpState getPlpState() {
        return plpState;
    }

    public void setPlpState(PlpState plpState) {
        this.plpState = plpState;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
