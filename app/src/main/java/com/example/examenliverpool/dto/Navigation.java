package com.example.examenliverpool.dto;

import java.util.List;

public class Navigation {
    private List<Current> current;

    private List<String> ancester;

    private List<String> childs;

    public List<Current> getCurrent() {
        return current;
    }

    public void setCurrent(List<Current> current) {
        this.current = current;
    }

    public List<String> getAncester() {
        return ancester;
    }

    public void setAncester(List<String> ancester) {
        this.ancester = ancester;
    }

    public List<String> getChilds() {
        return childs;
    }

    public void setChilds(List<String> childs) {
        this.childs = childs;
    }
}
