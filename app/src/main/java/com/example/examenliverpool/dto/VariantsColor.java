package com.example.examenliverpool.dto;

public class VariantsColor {
    private String colorName;

    private String colorImageURL;

    private String colorHex;

    public String getColorName ()
    {
        return colorName;
    }

    public void setColorName (String colorName)
    {
        this.colorName = colorName;
    }

    public String getColorImageURL ()
    {
        return colorImageURL;
    }

    public void setColorImageURL (String colorImageURL)
    {
        this.colorImageURL = colorImageURL;
    }

    public String getColorHex ()
    {
        return colorHex;
    }

    public void setColorHex (String colorHex)
    {
        this.colorHex = colorHex;
    }

}
