package com.example.examenliverpool.dto;

public class PlpState {

    private String lastRecNum;

    private String totalNumRecs;

    private String firstRecNum;

    private String currentSortOption;

    private String categoryId;

    private String recsPerPage;

    private String currentFilters;

    public String getLastRecNum() {
        return lastRecNum;
    }

    public void setLastRecNum(String lastRecNum) {
        this.lastRecNum = lastRecNum;
    }

    public String getTotalNumRecs() {
        return totalNumRecs;
    }

    public void setTotalNumRecs(String totalNumRecs) {
        this.totalNumRecs = totalNumRecs;
    }

    public String getFirstRecNum() {
        return firstRecNum;
    }

    public void setFirstRecNum(String firstRecNum) {
        this.firstRecNum = firstRecNum;
    }

    public String getCurrentSortOption() {
        return currentSortOption;
    }

    public void setCurrentSortOption(String currentSortOption) {
        this.currentSortOption = currentSortOption;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getRecsPerPage() {
        return recsPerPage;
    }

    public void setRecsPerPage(String recsPerPage) {
        this.recsPerPage = recsPerPage;
    }

    public String getCurrentFilters() {
        return currentFilters;
    }

    public void setCurrentFilters(String currentFilters) {
        this.currentFilters = currentFilters;
    }
}
