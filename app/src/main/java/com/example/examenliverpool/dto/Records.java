package com.example.examenliverpool.dto;

import java.util.List;

public class Records {
    private String isMarketPlace;

    private String maximumListPrice;

    private String groupType;

    private List<String> plpFlags;

    private String productId;

    private String smImage;

    private String minimumListPrice;

    private String xlImage;

    private String skuRepositoryId;

    private String productAvgRating;

    private String marketplaceBTMessage;

    private String promoPrice;

    private String minimumPromoPrice;

    private String productDisplayName;

    private String productRatingCount;

    private String isHybrid;

    private List<VariantsColor> variantsColor;

    private String maximumPromoPrice;

    private String isImportationProduct;

    private String lgImage;

    private String productType;

    private String listPrice;

    private String marketplaceSLMessage;

    public String getIsMarketPlace() {
        return isMarketPlace;
    }

    public void setIsMarketPlace(String isMarketPlace) {
        this.isMarketPlace = isMarketPlace;
    }

    public String getMaximumListPrice() {
        return maximumListPrice;
    }

    public void setMaximumListPrice(String maximumListPrice) {
        this.maximumListPrice = maximumListPrice;
    }

    public String getGroupType() {
        return groupType;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    public List<String> getPlpFlags() {
        return plpFlags;
    }

    public void setPlpFlags(List<String> plpFlags) {
        this.plpFlags = plpFlags;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getSmImage() {
        return smImage;
    }

    public void setSmImage(String smImage) {
        this.smImage = smImage;
    }

    public String getMinimumListPrice() {
        return minimumListPrice;
    }

    public void setMinimumListPrice(String minimumListPrice) {
        this.minimumListPrice = minimumListPrice;
    }

    public String getXlImage() {
        return xlImage;
    }

    public void setXlImage(String xlImage) {
        this.xlImage = xlImage;
    }

    public String getSkuRepositoryId() {
        return skuRepositoryId;
    }

    public void setSkuRepositoryId(String skuRepositoryId) {
        this.skuRepositoryId = skuRepositoryId;
    }

    public String getProductAvgRating() {
        return productAvgRating;
    }

    public void setProductAvgRating(String productAvgRating) {
        this.productAvgRating = productAvgRating;
    }

    public String getMarketplaceBTMessage() {
        return marketplaceBTMessage;
    }

    public void setMarketplaceBTMessage(String marketplaceBTMessage) {
        this.marketplaceBTMessage = marketplaceBTMessage;
    }

    public String getPromoPrice() {
        return promoPrice;
    }

    public void setPromoPrice(String promoPrice) {
        this.promoPrice = promoPrice;
    }

    public String getMinimumPromoPrice() {
        return minimumPromoPrice;
    }

    public void setMinimumPromoPrice(String minimumPromoPrice) {
        this.minimumPromoPrice = minimumPromoPrice;
    }

    public String getProductDisplayName() {
        return productDisplayName;
    }

    public void setProductDisplayName(String productDisplayName) {
        this.productDisplayName = productDisplayName;
    }

    public String getProductRatingCount() {
        return productRatingCount;
    }

    public void setProductRatingCount(String productRatingCount) {
        this.productRatingCount = productRatingCount;
    }

    public String getIsHybrid() {
        return isHybrid;
    }

    public void setIsHybrid(String isHybrid) {
        this.isHybrid = isHybrid;
    }

    public List<VariantsColor> getVariantsColor() {
        return variantsColor;
    }

    public void setVariantsColor(List<VariantsColor> variantsColor) {
        this.variantsColor = variantsColor;
    }

    public String getMaximumPromoPrice() {
        return maximumPromoPrice;
    }

    public void setMaximumPromoPrice(String maximumPromoPrice) {
        this.maximumPromoPrice = maximumPromoPrice;
    }

    public String getIsImportationProduct() {
        return isImportationProduct;
    }

    public void setIsImportationProduct(String isImportationProduct) {
        this.isImportationProduct = isImportationProduct;
    }

    public String getLgImage() {
        return lgImage;
    }

    public void setLgImage(String lgImage) {
        this.lgImage = lgImage;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getListPrice() {
        return listPrice;
    }

    public void setListPrice(String listPrice) {
        this.listPrice = listPrice;
    }

    public String getMarketplaceSLMessage() {
        return marketplaceSLMessage;
    }

    public void setMarketplaceSLMessage(String marketplaceSLMessage) {
        this.marketplaceSLMessage = marketplaceSLMessage;
    }
}
