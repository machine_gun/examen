package com.example.examenliverpool.ws;

import com.example.examenliverpool.dto.ResponseDTO;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RetrofitObjectAPI {

    @GET("plp")
    Call<ResponseDTO> getSearch(@Query("force-plp") String plp,
                                @Query("search-string") String criterio,
                                @Query("page-number") int pageNumber,
                                @Query("number-of-items-per-page") String itemPerPage);
}
