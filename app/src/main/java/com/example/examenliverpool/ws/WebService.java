package com.example.examenliverpool.ws;

import com.example.examenliverpool.dto.ResponseDTO;
import com.example.examenliverpool.utils.Interfaces;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WebService {

    private static final String url = "https://shoppapp.liverpool.com.mx/appclienteservices/services/v3/";

    public static void getSearch(final Interfaces.OnResponse<Object> onResponse, final int request, String criterio, int pageNumber, String itemPerPage) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RetrofitObjectAPI service = retrofit.create(RetrofitObjectAPI.class);

        Call<ResponseDTO> call = service.getSearch("true", criterio, pageNumber, itemPerPage);

        call.enqueue(new Callback<ResponseDTO>() {
            @Override
            public void onResponse(Call<ResponseDTO> call, Response<ResponseDTO> response) {

                int code = response.code();
                String message = response.message();

                if (code != 200) {
                    onResponse.onResponse(request, message);
                    return;
                }
                ResponseDTO object = response.body();
                onResponse.onResponse(request, object);
            }

            @Override
            public void onFailure(Call<ResponseDTO> call, Throwable t) {

                onResponse.onResponse(request, t.getMessage());
            }
        });
    }

}
