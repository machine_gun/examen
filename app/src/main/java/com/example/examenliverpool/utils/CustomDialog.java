package com.example.examenliverpool.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.example.examenliverpool.R;

public class CustomDialog {
    public static void show(final Context context, final Interfaces.OnResponse<Object> mOnResponse, final int dialogId, final String message) {
        final Dialog dialog = new Dialog(context, R.style.dialogFullScreen);

        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_choice);

        dialog.findViewById(R.id.tvDialogYes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnResponse.onResponse(dialogId, true);
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.tvDialogNo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnResponse.onResponse(dialogId, false);
                dialog.dismiss();
            }
        });

        ((TextView) dialog.findViewById(R.id.tvDialog)).setText(message);

        dialog.show();
    }
}
