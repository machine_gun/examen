package com.example.examenliverpool.utils;

import com.nostra13.universalimageloader.core.DisplayImageOptions;

public class Tools {
    public static DisplayImageOptions getImageLoaderOptions(int roundedRadious, int placeHolderResource) {
        return new DisplayImageOptions.Builder()
                .showImageOnLoading(placeHolderResource)
                .showImageForEmptyUri(placeHolderResource)
                .showImageOnFail(placeHolderResource)
                .considerExifParams(true)
                .displayer(new MyRoundedBitmapDisplayer(roundedRadious))
                .build();
    }
}
