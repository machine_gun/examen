package com.example.examenliverpool.utils;

public class Interfaces {

    public interface OnResponse<T> {
        void onResponse(int handlerCode, T t);
    }
}
